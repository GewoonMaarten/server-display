#!/usr/bin/env python

import curses, time, requests, os
import curses.textpad
import RPi.GPIO as GPIO

MS_NODE_1 = "http://192.168.178.58:80"
MS_NODE_2 = "http://192.168.178.55:3101"
MS_NODE_3 = "http://192.168.178.56:3101"
MS_NODE_4 = "http://192.168.178.57:3101"

class Display:
    def __enter__(self):
        self.stdscr = curses.initscr()
        curses.cbreak()
        curses.noecho()
        curses.start_color()
        curses.use_default_colors()
        curses.curs_set(0)
        self.stdscr.keypad(1)
        return self.stdscr
    def __exit__(self, type, value, traceback):
        curses.nocbreak()
        self.stdscr.keypad(0)
        curses.echo()
        curses.curs_set(1)
        curses.endwin()

    def get_response(self,ip):
        result = ""
        color = ""
        try:
            r = requests.get(ip)
            if (r.status_code >= 500):
                color = curses.color_pair(4)
            else:
                color = curses.color_pair(3)
            result = " %s  " % r.status_code
        except requests.exceptions.ConnectionError:
            result = " DOWN "
            color = curses.color_pair(4)

        return result, color

    def draw_screen(self,height, width, stdscr):
        stdscr.bkgd(' ', curses.color_pair(2))
        stdscr.insstr(0,0, " "*width, curses.color_pair(1))

        stdscr.insstr(2,0,"192.168.178.58 \"ms-node-1\" status: ", curses.color_pair(2) | curses.A_BOLD)
        stdscr.insstr(5,0,"192.168.178.55 \"ms-node-2\" status: ", curses.color_pair(2) | curses.A_BOLD)
        stdscr.insstr(8,0,"192.168.178.56 \"ms-node-3\" status: ", curses.color_pair(2) | curses.A_BOLD)
        stdscr.insstr(11,0,"192.168.178.57 \"ms-node-4\" status: ", curses.color_pair(2) | curses.A_BOLD)

        stdscr.insstr(height-1,0," "*width, curses.color_pair(1))

        result, color = self.get_response(MS_NODE_1)
        stdscr.addstr(2,34,result, color | curses.A_BOLD)

        result, color = self.get_response(MS_NODE_2)
        stdscr.addstr(5,34,result, color | curses.A_BOLD)

        result, color = self.get_response(MS_NODE_3)
        stdscr.addstr(8,34,result, color | curses.A_BOLD)

        result, color = self.get_response(MS_NODE_4)
        stdscr.addstr(11,34,result, color | curses.A_BOLD)

class Buttons:
    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(17, GPIO.IN, pull_up_down = GPIO.PUD_UP)
        os.system("sudo sh -c 'echo 508 > /sys/class/gpio/export'")
        os.system("sudo sh -c 'echo 'out' > /sys/class/gpio/gpio508/direction'")
        os.system("sudo sh -c 'echo '1' > /sys/class/gpio/gpio508/value'")
    def contents(self,filename):
    	with open(filename, 'r') as f:
    		return f.readline().strip()
    def sleep(self, channel):
    	value = self.contents("/sys/class/gpio/gpio508/value")

    	if(value == "1"):
    		os.system("sudo sh -c 'echo '0' > /sys/class/gpio/gpio508/value'")
    	elif(value == "0"):
    		os.system("sudo sh -c 'echo '1' > /sys/class/gpio/gpio508/value'")

def refresh_screen():
    with Display() as stdscr:
        height, width = stdscr.getmaxyx()

        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_BLUE)
        curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_GREEN)
        curses.init_pair(4, curses.COLOR_WHITE, curses.COLOR_RED)

        key = 'X'
        curses.halfdelay(5)

        while True:
            stdscr.clear()

            Display().draw_screen(height, width, stdscr)

            if key != ord('q'):
                key = stdscr.getch()
                stdscr.refresh()
                time.sleep(1)
            elif key == ord('q'):
                GPIO.cleanup()
                break

if __name__ == "__main__":
    buttons = Buttons()
    GPIO.add_event_detect(17, GPIO.FALLING, callback = buttons.sleep, bouncetime = 2000)
    refresh_screen()
